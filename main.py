from transformers import pipeline
import tkinter as tk
from tkinter import simpledialog, messagebox
import random

# Inicializar el pipeline de análisis de sentimientos
clasificador = pipeline('sentiment-analysis', model='nlptown/bert-base-multilingual-uncased-sentiment')


def analizar_sentimiento_hf(texto):
    resultado = clasificador(texto)
    label = resultado[0]['label']

    # Mapeo de la etiqueta del sentimiento a una emoción
    if label == '1 star':
        emotion = 'Sentimiento muy negativo'
    elif label == '2 stars':
        emotion = 'Sentimiento negativo'
    elif label == '3 stars':
        emotion = 'Sentimiento neutral'
    elif label == '4 stars':
        emotion = 'Sentimiento positivo'
    elif label == '5 stars':
        emotion = 'Sentimiento muy positivo'
    else:
        emotion = 'Sentimiento no identificado'

    return resultado, emotion


def mostrar_resultado(texto=None):
    if texto is None:
        # Obtener el texto ingresado por el usuario
        texto = simpledialog.askstring(
            title="Entrada",
            prompt="Introduce el texto para analizar el sentimiento:",
            parent=root,
        )

    if texto:
        # Realizar el análisis de sentimientos
        resultado, emocion = analizar_sentimiento_hf(texto)

        # Crear un mensaje con los resultados
        mensaje = f"Sentimiento: {resultado[0]['label']}\nEmoción predominante: {emocion}"

        # Mostrar los resultados en una ventana emergente
        messagebox.showinfo(
            title="Resultado del Análisis de Sentimientos",
            message=mensaje,
            parent=root,
        )

        # Actualizar el historial
        historial.insert(tk.END, f"Texto: {texto}\n{mensaje}\n\n")
        historial.see(tk.END)


def analizar_frase_seleccionada():
    # Obtener la frase seleccionada del Listbox
    selected_indices = listbox.curselection()
    if selected_indices:
        selected_index = selected_indices[0]
        texto = frases[selected_index]
        mostrar_resultado(texto)


# Crear una lista de frases inventadas de 100 tokens
frases = [
    "El sol brillaba intensamente sobre el vasto océano mientras las olas rompían suavemente en la orilla, creando un paisaje de serenidad que invitaba a la reflexión y al descanso. En la distancia, se podían ver las siluetas de los barcos navegando pacíficamente, completando el cuadro perfecto de un día de verano en la playa.",
    "En el corazón de la ciudad, los edificios se elevaban majestuosamente hacia el cielo, reflejando la luz del sol en sus ventanas de vidrio. Las calles estaban llenas de gente apresurada, cada una con su propio destino, creando una sinfonía de sonidos urbanos que se mezclaban en un ritmo constante e imparable.",
    "El bosque era un lugar de maravillas, donde los árboles antiguos se erguían como gigantes protectores, y el suelo estaba cubierto de una alfombra de hojas crujientes. Los rayos de sol filtraban a través del dosel, creando patrones de luz y sombra que bailaban con el viento, mientras los animales se movían furtivamente entre los arbustos.",
    "La biblioteca era un santuario de conocimiento, con estanterías que se extendían hasta el techo, repletas de libros que contenían historias y sabiduría de épocas pasadas. El silencio reinaba en el ambiente, solo interrumpido por el susurro de las páginas al pasar, creando una atmósfera de calma y concentración.",
    "El mercado estaba lleno de vida y color, con puestos que ofrecían una variedad infinita de productos frescos, desde frutas y verduras hasta especias y artesanías. Los vendedores anunciaban sus mercancías con entusiasmo, y los clientes regateaban alegremente, disfrutando del bullicio y la energía del lugar."
]

# Asegurarse de que cada frase tenga exactamente 100 palabras
frases = [" ".join(frase.split()[:100]) for frase in frases]


# Crear la ventana principal de Tkinter
root = tk.Tk()
root.title("Análisis de Sentimientos")
root.geometry("700x600")
root.configure(bg="#f0f0f0")

# Configurar estilos personalizados
style = {
    "padx": 20,
    "pady": 20,
    "font": ("Helvetica", 14),
    "bg": "#ffffff",
    "fg": "#333333"
}

# Botón para mostrar la ventana emergente de entrada de texto
btn_analizar = tk.Button(
    root,
    text="Analizar Sentimiento",
    command=mostrar_resultado,
    font=("Helvetica", 16),
    bg="#4CAF50",
    fg="white",
    padx=20,
    pady=10
)

# Ubicar el botón en la ventana principal
btn_analizar.pack(pady=10)

# Crear un widget Text para el historial de consultas
historial = tk.Text(root, height=10, width=80, font=("Helvetica", 12))
historial.pack(pady=10)

# Crear un Listbox para las frases inventadas
listbox = tk.Listbox(root, height=10, width=80, font=("Helvetica", 12))
for frase in frases:
    listbox.insert(tk.END, frase)
listbox.pack(pady=10)

# Botón para analizar la frase seleccionada del Listbox
btn_analizar_seleccionada = tk.Button(
    root,
    text="Analizar Frase Seleccionada",
    command=analizar_frase_seleccionada,
    font=("Helvetica", 16),
    bg="#2196F3",
    fg="white",
    padx=20,
    pady=10
)

# Ubicar el botón en la ventana principal
btn_analizar_seleccionada.pack(pady=10)

# Ejecutar el bucle principal de Tkinter
root.mainloop()
